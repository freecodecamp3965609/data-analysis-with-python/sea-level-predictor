import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import linregress

def draw_plot():
    # Read data from file
    sea=pd.read_csv('epa-sea-level.csv', index_col='Year')


    # Create scatter plot
    x = sea.index
    y = sea['CSIRO Adjusted Sea Level']
    plt.scatter(x, y, marker='+')

    # Create first line of best fit
    X_full = pd.Series([i for i in range(1880, 2051)])
    lr1 = linregress(x,y)
    plt.plot(X_full, lr1.intercept + lr1.slope*X_full)

    # Create second line of best fit
    X_short = pd.Series([i for i in range(2000, 2014)])
    Y_short = y[-len(X_short):]
    X_long = pd.Series([i for i in range(2000, 2051)])
    lr2 = linregress(X_short,Y_short)
    plt.plot(X_long, lr2.intercept + lr2.slope*X_long)

    # Add labels and title
    plt.title('Rise in Sea Level')
    plt.xlabel('Year')
    plt.ylabel('Sea Level (inches)')
    plt.xlim(1850, 2075)
    
    # Save plot and return data for testing (DO NOT MODIFY)
    plt.savefig('sea_level_plot.png')
    return plt.gca()